#ifndef _INFO_H
#define _INFO_H

#include <windows.h>
#include <string>
#include <iostream>

using namespace std;
typedef unsigned long uint32;

// ������������ ���� ������ Windows �� ������� ������
enum CWindowsVersion {
    WINDOWS_UNKNOWN = 0,    // ���������� ������ �� �������
    WINDOWS_OLD,            // ����� ������, �������� Windows 3.1
    WINDOWS_95,
    WINDOWS_95_OSR2,
    WINDOWS_98,
    WINDOWS_98_SE,
    WINDOWS_ME,
    WINDOWS_NT4,
    WINDOWS_2000,
    WINDOWS_XP,
    WINDOWS_2003,
    WINDOWS_VISTA,
    WIMDOWS_FUTURE          // ��������������� ��� ���� ��� ������������ ������
};
// ������������ ���������� �����������
enum CProcessorArchitecture {
    UNKNOWN = 0,    // �� ������� ����������
    X86,
    IA64,
    AMD64,
    X86_WIN64       // ��������� �86, �� Windows 64-��������� ??
};

// ��������� ���������� ����� ���������� �� ������������ ��������
struct COSVersion {
    uint32 MajorVersion;   // ������� ����� ������
    uint32 MinorVersion;   // ������� ����� ������
    uint32 Build;          // ����� ������
    string CSDVersion;     // ������ ���������� �������� ������ ��
    // �������� 'Service Pack 2'
    CWindowsVersion WindowsVersion; // ������������ ���������� ������ ��

    COSVersion() : MajorVersion(0),
                   MinorVersion(0),
                   Build(0),
                   CSDVersion(),
                   WindowsVersion(WINDOWS_UNKNOWN) {
    };

};

// ����� ����� ��������� ����������
class CSystemInformation {
private:
    COSVersion m_st_WinVersion;            // ��������� ����� ���������� �� ��
    uint32 m_ui32_TotalSystemMemory;   // ����� ����� ��������� ������
    uint32 m_ui32_TotalPhysicalMemory; // ����� ����� ���������� ������
    uint32 m_ui32_TotalPageFile;
    uint32 m_ui32_FreeSystemMemory;    // ��������� ����� ��������� ������
    uint32 m_ui32_FreePhysicalMemory;  // ��������� ����� ���������� ������
    uint32 m_ui32_ProcessorNum;        // ���������� �����������
    uint32 m_ui32_ProcessorType;
    CProcessorArchitecture m_en_ProcessorArchitecture; // ����������� ����������
    string m_str_WindowsVersion;       // ������ ����� ���������� � ������ ��
    // �������� ���������� � ��������� ������ �������
    void UpdateMemoryInfo() {
        MEMORYSTATUS st_MemoryInfo;
        memset(&st_MemoryInfo, 0x00, sizeof(MEMORYSTATUS));
        // ��������� �� �� ������ � ��������� ������
        GlobalMemoryStatus(&st_MemoryInfo);
        m_ui32_FreeSystemMemory = static_cast <uint32> (st_MemoryInfo.dwAvailVirtual);
        m_ui32_FreePhysicalMemory = static_cast <uint32> (st_MemoryInfo.dwAvailPhys);
    };


public:
    // �����������
    CSystemInformation() : m_st_WinVersion(),
                           m_ui32_TotalSystemMemory(0),
                           m_ui32_TotalPhysicalMemory(0),
                           m_ui32_TotalPageFile(0),
                           m_ui32_FreeSystemMemory(0),
                           m_ui32_FreePhysicalMemory(0),
                           m_ui32_ProcessorNum(0),
                           m_en_ProcessorArchitecture(UNKNOWN),
                           m_str_WindowsVersion() {
        SYSTEM_INFO st_SysInfo;     // ��������� ����������
        OSVERSIONINFO st_OsInfo;      // ���������� �� ��
        MEMORYSTATUS st_MemoryInfo;  // ���������� � ������
        memset(&st_SysInfo, 0x00, sizeof(SYSTEM_INFO));
        memset(&st_OsInfo, 0x00, sizeof(OSVERSIONINFO));
        memset(&st_MemoryInfo, 0x00, sizeof(MEMORYSTATUS));
        st_OsInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
        // ��������� �� �� ���������� � ������
        GlobalMemoryStatus(&st_MemoryInfo);
        m_ui32_TotalSystemMemory = static_cast <uint32> (st_MemoryInfo.dwTotalVirtual);
        m_ui32_TotalPhysicalMemory = static_cast <uint32> (st_MemoryInfo.dwTotalPhys);
        // ��������� �� �� ���������� � ������������ ����������
        GetSystemInfo(&st_SysInfo);
        // ��������� ������ ��
        GetVersionEx(&st_OsInfo);
        // ��������� ���������� �����������
        m_ui32_ProcessorNum = st_SysInfo.dwNumberOfProcessors;
        m_ui32_ProcessorType = st_SysInfo.dwProcessorType;
        // ��������� ����������� ����������
        switch (st_SysInfo.wProcessorArchitecture) {
            case PROCESSOR_ARCHITECTURE_INTEL: {
                m_en_ProcessorArchitecture = X86;
                break;
            };
            case PROCESSOR_ARCHITECTURE_IA64: {
                m_en_ProcessorArchitecture = IA64;
                break;
            };
            case PROCESSOR_ARCHITECTURE_AMD64: {
                m_en_ProcessorArchitecture = AMD64;
                break;
            };
            case PROCESSOR_ARCHITECTURE_IA32_ON_WIN64: {
                m_en_ProcessorArchitecture = X86_WIN64;
                break;
            };
            default: {
                m_en_ProcessorArchitecture = UNKNOWN;
                break;
            };
        };
        // ��������� ������ ��
        m_st_WinVersion.MajorVersion = st_OsInfo.dwMajorVersion; // ����� ������ ��
        m_st_WinVersion.MinorVersion = st_OsInfo.dwMinorVersion; //
        m_st_WinVersion.Build = st_OsInfo.dwBuildNumber;  // ����� ������ ��
        st_OsInfo.szCSDVersion[127] = 0x00; // ��������� ���������� ������� �������
        // ��� ����������� �������������� � ������
        m_st_WinVersion.CSDVersion = &st_OsInfo.szCSDVersion[0];
        // ���������� ��������� �� ������ ������ ��� MS Windows
        m_str_WindowsVersion = "Microsoft Windows ";
        // ��������� ������ �� � ������������ � ������
        switch (st_OsInfo.dwPlatformId) {
            case VER_PLATFORM_WIN32_NT: {
                m_st_WinVersion.WindowsVersion = WIMDOWS_FUTURE;
                if (st_OsInfo.dwMajorVersion == 4) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_NT4;
                    m_str_WindowsVersion = m_str_WindowsVersion + "NT4 ";
                };
                if ((st_OsInfo.dwMajorVersion == 5) && (st_OsInfo.dwMinorVersion == 0)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_2000;
                    m_str_WindowsVersion = m_str_WindowsVersion + "2000 ";
                };
                if ((st_OsInfo.dwMajorVersion == 5) && (st_OsInfo.dwMinorVersion == 1)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_XP;
                    m_str_WindowsVersion = m_str_WindowsVersion + "XP ";
                };
                if ((st_OsInfo.dwMajorVersion == 5) && (st_OsInfo.dwMinorVersion == 2)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_2003;
                    m_str_WindowsVersion = m_str_WindowsVersion + "2003 ";
                };
                if ((st_OsInfo.dwMajorVersion == 6) && (st_OsInfo.dwMinorVersion == 0)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_VISTA;
                    m_str_WindowsVersion = m_str_WindowsVersion + "Vista ";
                };
                break;
            };
            case VER_PLATFORM_WIN32_WINDOWS: {
                m_st_WinVersion.WindowsVersion = WINDOWS_OLD;
                if ((st_OsInfo.dwMajorVersion == 4) && (st_OsInfo.dwMinorVersion == 0)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_95;
                    m_str_WindowsVersion = m_str_WindowsVersion + "95 ";
                    if ((st_OsInfo.szCSDVersion[1] == 'C') || (st_OsInfo.szCSDVersion[1] == 'B')) {
                        m_st_WinVersion.WindowsVersion = WINDOWS_95_OSR2;
                        m_str_WindowsVersion = m_str_WindowsVersion + "OSR2 ";
                    };
                };
                if ((st_OsInfo.dwMajorVersion == 4) && (st_OsInfo.dwMinorVersion == 10)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_98;
                    m_str_WindowsVersion = m_str_WindowsVersion + "98 ";
                    if (st_OsInfo.szCSDVersion[1] == 'A') {
                        m_st_WinVersion.WindowsVersion = WINDOWS_98_SE;
                        m_str_WindowsVersion = m_str_WindowsVersion + "SE ";
                    };
                };
                if ((st_OsInfo.dwMajorVersion == 4) && (st_OsInfo.dwMinorVersion == 90)) {
                    m_st_WinVersion.WindowsVersion = WINDOWS_ME;
                    m_str_WindowsVersion = m_str_WindowsVersion + "Me ";
                };
                break;
            };
            case VER_PLATFORM_WIN32s: {
                m_st_WinVersion.WindowsVersion = WINDOWS_OLD;
                m_str_WindowsVersion = m_str_WindowsVersion + "3.1 or older ";
                break;
            };
            default: {
                m_st_WinVersion.WindowsVersion = WINDOWS_UNKNOWN;
                m_str_WindowsVersion = m_str_WindowsVersion + "unknown version ";
            };
        };
        // ������������ ������ ������ � ������� ��
        // IntToStr ����������� ������� ��������� ����� ����� � string`�
        m_str_WindowsVersion = m_str_WindowsVersion;
        // �������� ������ � ��������� ������ ��
        UpdateMemoryInfo();
    };

    // ����������
    virtual ~CSystemInformation() {
    };

    // �������� ����������
    void Update() {
        UpdateMemoryInfo();
    };

    // �������� ������ �� �� ������������
    CWindowsVersion GetWindowsVersion() {
        return m_st_WinVersion.WindowsVersion;
    };

    // �������� ����� ��������� ������
    uint32 GetTotalSystemMemorySize() {
        return m_ui32_TotalSystemMemory;
    };

    // �������� ����� ���������� ������
    uint32 GetTotalPhysicalMemorySize() {
        return m_ui32_TotalPhysicalMemory;
    };

    // �������� ��������� ����� ��������� ������
    uint32 GetFreeMemorySize() {
        return m_ui32_FreeSystemMemory;
    };

    // �������� ��������� ����� ���������� ������
    uint32 GetFreePhysicalMemorySize() {
        return m_ui32_FreePhysicalMemory;
    };

    // �������� ������ ���������� � ������ ��
    const char *GetFullWindowsVersion() const {
        return m_str_WindowsVersion.c_str();
    };

    // �������� ������ ���������� � ������ ��
    string GetFullWindowsVersionEx() {
        return m_str_WindowsVersion;
    };
};

int main() {
    CSystemInformation *pSystemInformation = new CSystemInformation();
    cout << pSystemInformation->GetWindowsVersion() \
         << ' ' << \
         pSystemInformation->GetTotalPhysicalMemorySize();
    Sleep(10000);
    return 0;
}

#endif /* _INFO_H */
